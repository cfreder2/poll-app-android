package com.c9ws.pollapp;

import android.util.Log;

public class HttpResultObject {
	private String action, RID, PID, data;
	private AsyncHTTPGetTask task;
	private String resultString;

	public HttpResultObject() {
		action = null;
		RID = null;
		PID = null;
		data = null;
	}

	public HttpResultObject(String action, String RID, String PID, String data) {
		this.action = action;
		this.RID = RID;
		this.PID = PID;
		this.data = data;
	}
	
	public void clear()
	{
		action = null;
		RID = null;
		PID = null;
		data = null;
	}

	public String execute() {
		return execute(this.action, this.RID, this.PID, this.data);
	}
	
	public String execute(String action, String RID, String PID, String data) {
		String temp = "CONN_TIME_OUT";
		while(temp.equals("CONN_TIME_OUT")) {
			temp = execute2(action, RID, PID, data);
		}
		return temp;
	}
	
	public String execute2(String action, String RID, String PID, String data) {
		String reqString = "http://poll.c9ws.com/app/app.php?a=" + action;
		if (RID != null)
			reqString += ("&rid=" + RID);
		if (PID != null)
			reqString += ("&pid=" + PID);
		if (data != null)
			reqString += ("&d=" + data);

		Log.i("reqString", reqString);
		
		reqString = reqString.replace(" ", "_");
		String result = new URLReader(reqString).read();
		if (result != "ERROR") {
			return result;
		} else {
			return "CONN_TIME_OUT";
		}
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRID() {
		return RID;
	}

	public void setRID(String rID) {
		RID = rID;
	}

	public String getPID() {
		return PID;
	}

	public void setPID(String pID) {
		PID = pID;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public AsyncHTTPGetTask getTask() {
		return task;
	}

	public void setTask(AsyncHTTPGetTask task) {
		this.task = task;
	}

	public String getResultString() {
		return resultString;
	}

	public void setResultString(String resultString) {
		this.resultString = resultString;
	}

}
