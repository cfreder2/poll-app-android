package com.c9ws.pollapp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import android.util.Log;

public class URLReader {

	private String uri;

	public URLReader(String target) {
		this.uri = target;
	}

	public String read() {
		URL u;
		try {
			u = new URL(this.uri);
			Log.i("URL", u.toString());
			BufferedReader in = new BufferedReader(new InputStreamReader(
					u.openStream()));
			String ret = in.readLine();
			Log.i("READ()", ret);
			return ret;
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}
}
