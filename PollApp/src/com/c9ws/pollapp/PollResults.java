package com.c9ws.pollapp;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PollResults extends Activity {
	private TextView roomName, currentPollQuestion;
	private ListView resultsList;
	private String rn = "";
	private ArrayList<String> pollAL = new ArrayList<String>();
	private ArrayAdapter<String> adapter;
	private int tvotes = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.poll_results);

		roomName = (TextView) findViewById(R.id.set_room_name2);
		currentPollQuestion = (TextView) findViewById(R.id.set_current_poll_question2);
		resultsList = (ListView) findViewById(R.id.results_list);

		Intent i = getIntent();
		this.rn = i.getStringExtra("roomname");
		roomName.setText("Room: " + this.rn);
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, pollAL);
		resultsList.setAdapter(adapter);

		update();
	}

	private void update() {
		this.tvotes = 0;
		ArrayList<String> temp = new ArrayList<String>();
		pollAL.clear();
		HttpResultObject resultObj = new HttpResultObject("rp", this.rn, null,
				null);
		String polldata = resultObj.execute();
		Log.i("Poll Content", polldata);
		String[] subs = polldata.split("\\|");
		Log.i("ArrayContents", java.util.Arrays.toString(subs));
		for (String s : subs) {
			temp.add(s);
		}
		String q = temp.get(0);
		if (q.equals("NOPOLLYETCREATED")) {
			q = "Please wait, the Poll Master is creating a poll.";
		}
		currentPollQuestion.setText("Q: " + q);
		temp.remove(0);
		
		Boolean flip = false;
		for (String c : temp) {
			if (!flip) {
				flip = true;
			} else {
				this.tvotes += Integer.parseInt(c);
				flip = false;
			}
		}

		flip = false;
		String t2 = "";
		for (String t : temp) {
			if (!flip) {
				t2 = t;
				flip = true;
			} else {
				Double tmp = Double.parseDouble(t);
				double p;
				String p2;
				if(tmp > 0) {
					p = (tmp / this.tvotes)*100;
					DecimalFormat df = new DecimalFormat("#.##");
					p2 = df.format(p);
					Log.i("VOTES", "T: " + this.tvotes + " | TMP: " + tmp + " | P: " + p2);
				} else {
					p2 = "0";
				}
				t2 += "\n(" + t + " Votes - " + p2 + "%)";
				pollAL.add(t2);
				t2 = "";
				flip = false;
			}
		}

		adapter.notifyDataSetChanged();
		Toast.makeText(getApplicationContext(), "Poll results updated.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.poll_results, menu);
		return true;
	}

	public void castVote(View v) {
		Intent i = new Intent(getApplicationContext(), RoomGUI.class);
		i.putExtra("roomname", this.rn);
		startActivity(i);
	}

	public void update(View v) {
		this.update();
	}

	public void roomList(View v) {
		Intent i = new Intent(this, JoinAPollRoom.class);
		startActivity(i);
	}
}
