package com.c9ws.pollapp;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class JoinAPollRoom extends Activity {
	

	private ListView roomList;
	private ArrayAdapter<String> adapter;
	private ArrayList<String> roomArrayList;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy); 
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_a_poll_room);
        
        roomList = (ListView) findViewById(R.id.roomList);
        roomArrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,  roomArrayList);
        populateRoomList();
        roomList.setAdapter(adapter);
        
		roomList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String text = ((TextView) view).getText().toString();
				Intent i = new Intent(getApplicationContext(), PollResults.class);
				i.putExtra("roomname", text);
				startActivity(i);
			}
		});
        
    }
    
    private void populateRoomList()
    {
    	String action = "lr";
    	HttpResultObject resultObj = new HttpResultObject(action, null, null, null);
    	String rooms = resultObj.execute();
    	Log.i("Rooms Content", rooms);    	
    	String[] subs = rooms.split("\\|");    	
    	Log.i("ArrayContents", java.util.Arrays.toString(subs));
    	for(String s : subs) {
    		roomArrayList.add(s);
    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.join_a_poll_room, menu);
        return true;
    }
    /*
     * 
     * generate a new poll room with this user as poll leader
     */
    public void createPoll(View view)
    {
    	Intent intent = new Intent(this, PollRoomCreator.class);
    	startActivity(intent);
    }
    
    public void refreshList(View view) {
    	roomArrayList.clear();
    	populateRoomList();
    	adapter.notifyDataSetChanged();
    }
}
