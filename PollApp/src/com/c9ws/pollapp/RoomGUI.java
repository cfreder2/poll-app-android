package com.c9ws.pollapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class RoomGUI extends Activity {

	private TextView roomName;
	private TextView currentPollQuestion;
	private RadioGroup rGroup;
	private RadioButton[] rbArray;
	private String roomID;
	private String pollID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.room_gui);

		roomName = (TextView) findViewById(R.id.set_room_name1);
		currentPollQuestion = (TextView) findViewById(R.id.poll_question);
		rGroup = (RadioGroup) findViewById(R.id.rGroup);
		rbArray = new RadioButton[4];
		rbArray[0] = (RadioButton) findViewById(R.id.rbutton1);
		rbArray[1] = (RadioButton) findViewById(R.id.rbutton2);
		rbArray[2] = (RadioButton) findViewById(R.id.rbutton3);
		rbArray[3] = (RadioButton) findViewById(R.id.rbutton4);
		Intent intent = getIntent();
		roomID = (String) intent.getStringExtra("roomname"); // needs to be
																// fixed
		roomName.setText(roomID);
		getPoll();

	}

	private void getPoll() {
		HttpResultObject resultObj = new HttpResultObject();

		resultObj.setAction("rp");
		resultObj.setRID(roomID);
		String poll = resultObj.execute();
		String[] results = poll.split("\\|");
		currentPollQuestion.setText(results[0]);
		for (int i = 1; i < results.length; i++) {
			if (i % 2 == 1) {
				rbArray[(i - 1) / 2].setText(results[i]);
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.room_gui, menu);
		return true;
	}

	public void castVote(View view) {
		Intent intent = new Intent(this, PollResults.class);
		int checked = rGroup.getCheckedRadioButtonId();
		HttpResultObject obj = new HttpResultObject("lpid", roomID, null, null);
		pollID = obj.execute();
		obj.clear();
		obj.setAction("v");
		obj.setPID(pollID);
		
		int index = rGroup.indexOfChild(findViewById(rGroup.getCheckedRadioButtonId()));
		
		obj.setData(Integer.toString(index));
		obj.execute();

		// HttpResultObject resultObj = new HttpResultObject("v", null, pollID,
		// new Integer(checked).toString()));

		intent.putExtra("roomname", roomID);
		startActivity(intent);
	}
}
