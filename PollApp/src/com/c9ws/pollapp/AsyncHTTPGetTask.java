package com.c9ws.pollapp;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

public class AsyncHTTPGetTask extends AsyncTask<String, Void, String> {
    private String result;


	@Override
	protected String doInBackground(String... params) {
		// TODO http request,
		//fix return type
		
		AndroidHttpClient client = AndroidHttpClient.newInstance("");
		HttpGet request = new HttpGet(params[0]);
		Log.i("AsyncHTTPGetTask", "URL: "+params[0]);
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		try 
		{
			return client.execute(request, responseHandler);
		} 
		catch (ClientProtocolException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO decide whether its a list or a string and set
		Log.i("OPE Result", result);
		super.onPostExecute(result);
	}
	
	public String getResultString()
	{
	    return this.result;
	}
	

}
