package com.c9ws.pollapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class PollRoomCreator extends Activity {
	private EditText roomName, question;
	private EditText response[];
	private String pollID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poll_room_creator);
        
        response = new EditText[4];
        roomName = (EditText) findViewById(R.id.editText_room_name);
        question = (EditText) findViewById(R.id.editText_question);
        response[0] = (EditText) findViewById(R.id.editText_response1);
        response[1] = (EditText) findViewById(R.id.editText_response2);
        response[2] = (EditText) findViewById(R.id.editText_response3);
        response[3] = (EditText) findViewById(R.id.editText_response4);
        
    }
    private void createPoll()
    {
    	HttpResultObject resultObj = new HttpResultObject();
    	resultObj.setAction("cr"); //create room
    	resultObj.setData(roomName.getText().toString());
    	resultObj.execute();
    	resultObj.clear();
    	
    	resultObj.setAction("cp"); //create poll
    	resultObj.setRID(roomName.getText().toString());
    	String[] temp = (resultObj.execute()).split("\\|");
    	pollID = temp[1];
    	Log.i("pollId", pollID);
    	resultObj.clear();
    	
    	resultObj.setAction("spq"); //set poll question
    	resultObj.setPID(pollID);
    	resultObj.setData(question.getText().toString());
    	resultObj.execute();
    	resultObj.clear();
    	
    	for (int i = 0; i < 4; i++)
    	{
    		resultObj.setAction("apr"); //adding poll responses
    		resultObj.setPID(pollID);
    		resultObj.setData(response[i].getText().toString());
    		resultObj.execute();
    		resultObj.clear();
    	}
    	resultObj.setAction("cm"); //commit the finished poll to list
    	resultObj.setPID(pollID);
    	resultObj.execute();    	
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.poll_room_creator, menu);
        return true;
    }
    
    public void activatePoll(View view)
    {
    	createPoll();
    	Intent intent = new Intent(this, JoinAPollRoom.class);
      	startActivity(intent);
    }
}
